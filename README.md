# X24 Test Project | Guilherme Burger Schalch

## Installation

#### 1- NPM

Run `npm install` command to install all dependencies.

#### 2- Environment

Remove the suffix "-example" from `source/config/env-example.js` file and add the API URL at the apiURL attribute inside it.
( `https://jsonplaceholder.typicode.com/` )

#### 3- Gulp

Run `gulp` command to generate Build folder and it will automatically start browser sync after completing. Voilá! :)

## Style Guide!

For project's styleguide, access: /styleguide.html in project root.
